﻿#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
// before receipt validation will compile in this sample.



#define RECEIPT_VALIDATION
#endif


using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;



using UnityEngine.UI;
using UnityEngine.Purchasing.Security;


using LitJson;



public class Purchaser : MonoBehaviour, IStoreListener
{
	string _day="day",_year="year",_month="month",_cname="cname",_pname="pname",_emailaddress="emailaddress",_add="add";

	bool canupload=false;

	private static IStoreController m_StoreController;                                                                  // Reference to the Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider;                                                         // Reference to store-specific Purchasing subsystems.

	// Product identifiers for all products capable of being purchased: "convenience" general identifiers for use with Purchasing, and their store-specific identifier counterparts 
	// for use with and outside of Unity Purchasing. Define store-specific identifiers also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

	private static string kProductIDConsumable =    "consumable";                                                         // General handle for the consumable product.
	private static string kProductIDNonConsumable = "nonconsumable";                                                  // General handle for the non-consumable product.
	private static string kProductIDSubscription =  "subscription";
	private static string kProductIDSubscription24 =  "Pack24";
	private static string kProductIDSubscription36 =  "Pack36";
	// General handle for the subscription product.

	private static string kProductNameAppleConsumable =    "com.unity3d.test.services.purchasing.consumable";             // Apple App Store identifier for the consumable product.
	private static string kProductNameAppleNonConsumable = "com.doggietalkie.myapp.freevid";      // Apple App Store identifier for the non-consumable product.
	private static string kProductNameAppleSubscription =  "com.doggietalkie.myapps.pack12";  
	private static string kProductNameAppleSubscription24 =  "com.doggietalkie.myapps.pack24";   
	private static string kProductNameAppleSubscription36 =  "com.doggietalkie.myapps.pack36";   

	// Apple App Store identifier for the subscription product.


	private static string kProductNameGooglePlayConsumable =    "com.unity3d.test.services.purchasing.consumable";        // Google Play Store identifier for the consumable product.
	private static string kProductNameGooglePlayNonConsumable = "com.doggietalkie.myapp.freevid";     // Google Play Store identifier for the non-consumable product.
	private static string kProductNameGooglePlaySubscription =  "com.doggietalkie.myapps.pack12";
	private static string kProductNameGooglePlaySubscription24 =  "com.doggietalkie.myapps.pack24";
	private static string kProductNameGooglePlaySubscription36 =  "com.doggietalkie.myapps.pack36";
//	private static string kProductNameGooglePlaySubscription =  "pack12";// Google Play Store identifier for the subscription product.

	public string expDate;
	public string PackName;
	public string packdeatils;

	WWW www;

	#if RECEIPT_VALIDATION
	private CrossPlatformValidator validator;
	#endif



	public	string setpackname;



	public Text child;
	public Text pname;
	public Text email;
	public Text add;
	public Text day;
	public Text month;
	public Text year;


	public Text scode;
	public string Platforms;


	public	string schooolscode;

	public Text[]	txtstobecheck;
	void Start()
	{
		packdeatils=",wording,application,thinking";
		setpackname = "pack36";


		// If we haven't set up the Unity Purchasing reference
		if (m_StoreController == null)
		{
			// Begin to configure our connection to Purchasing
			InitializePurchasing();
		}




	}

	public void InitializePurchasing() 
	{
		// If we have already connected to Purchasing ...
		if (IsInitialized())
		{
			// ... we are done here.
			return;
		}

		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		// Add a product to sell / restore by way of its identifier, associating the general identifier with its store-specific identifiers.
		builder.AddProduct(kProductIDConsumable, ProductType.Consumable, new IDs(){{ kProductNameAppleConsumable,       AppleAppStore.Name },{ kProductNameGooglePlayConsumable,  GooglePlay.Name },});// Continue adding the non-consumable product.
		builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable, new IDs(){{ kProductNameAppleNonConsumable,       AppleAppStore.Name },{ kProductNameGooglePlayNonConsumable,  GooglePlay.Name },});// And finish adding the subscription product.
		builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){{ kProductNameAppleSubscription,       AppleAppStore.Name },{ kProductNameGooglePlaySubscription,  GooglePlay.Name },});// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.

		builder.AddProduct(kProductIDSubscription24, ProductType.Subscription, new IDs(){{ kProductNameAppleSubscription24,       AppleAppStore.Name },{ kProductNameGooglePlaySubscription24,  GooglePlay.Name },});// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.



		builder.AddProduct(kProductIDSubscription36, ProductType.Subscription, new IDs(){{ kProductNameAppleSubscription36,       AppleAppStore.Name },{ kProductNameGooglePlaySubscription36,  GooglePlay.Name },});// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.

		UnityPurchasing.Initialize(this, builder);


		#if RECEIPT_VALIDATION
		validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.bundleIdentifier);
		#endif
	}


	private bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}


	public void BuyConsumable()
	{
		canupload = true;
		// Buy the consumable product using its general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
		BuyProductID(kProductIDConsumable);
	}


	public void BuyNonConsumable()
	{

		canupload = true;
		// Buy the non-consumable product using its general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
		BuyProductID(kProductIDNonConsumable);
	}


	public void BuySubscription()
	{

		canupload = true;
		// Buy the subscription product using its the general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
		BuyProductID(kProductIDSubscription);
	}


	public void BuySubscription24()
	{

		canupload = true;
		// Buy the subscription product using its the general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
		BuyProductID(kProductIDSubscription24);
	}


	public void BuySubscription36(){


		canupload = true;
		BuyProductID(kProductIDSubscription36);
	}



	void BuyProductID(string productId)
	{
		// If the stores throw an unexpected exception, use try..catch to protect my logic here.
		try
		{
			// If Purchasing has been initialized ...
			if (IsInitialized())
			{
				// ... look up the Product reference with the general product identifier and the Purchasing system's products collection.
				Product product = m_StoreController.products.WithID(productId);

				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase)
				{
					Debug.Log (string.Format("Purchasing product asychronously: '{0}'", product.definition.id));// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
					m_StoreController.InitiatePurchase(product);
				}
				// Otherwise ...
				else
				{
					// ... report the product look-up failure situation  
					Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			}
			// Otherwise ...
			else
			{
				// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or retrying initiailization.
				Debug.Log("BuyProductID FAIL. Not initialized.");
			}
		}
		// Complete the unexpected exception handling ...
		catch (Exception e)
		{
			// ... by reporting any unexpected exception for later diagnosis.
			Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
		}
	}


	// Restore purchases previously made by this customer. Some platforms automatically restore purchases. Apple currently requires explicit purchase restoration for IAP.
	public void RestorePurchases()
	{

		Debug.Log ("RestorePurchases");
		// If Purchasing has not yet been set up ...
		if (!IsInitialized())
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}


	//  
	// --- IStoreListener
	//

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log("OnInitialized: PASS");



		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;
	}


	public void OnInitializeFailed(InitializationFailureReason error)
	{
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) 
	{


		Debug.Log ("PurchaseProcessingResult");

		Debug.Log("transactionID :"+e.purchasedProduct.transactionID.ToString());
		Debug.Log( "receipt"+e.purchasedProduct.receipt.ToString ());



		Debug.Log("Purchase OK: " + e.purchasedProduct.definition.id);
		//		Debug.Log("Receipt: " + e.purchasedProduct.receipt);


		string payload=JsonMapper.ToObject<rec> (e.purchasedProduct.receipt).Payload;









		#if RECEIPT_VALIDATION
		if (Application.platform == RuntimePlatform.Android ||
			Application.platform == RuntimePlatform.IPhonePlayer ||
			Application.platform == RuntimePlatform.OSXPlayer) {
			try {
				var result = validator.Validate(e.purchasedProduct.receipt);
				Debug.Log("Receipt is valid. Contents:");



				Debug.Log("Total"+result[result.Length-1]);
				//	Debug.Log("Total"+result[18].productID);


				Debug.Log("Total aa"+result.Length);

				if(result[result.Length-1].productID==kProductNameAppleSubscription){


					PackName="Pack12";

				}else if(result[result.Length-1].productID==kProductNameAppleSubscription24){
					PackName="Pack24";


				}else if(result[result.Length-1].productID==kProductNameAppleSubscription36){

					PackName="Pack36";
				} else if(result[result.Length-1].productID==kProductNameAppleNonConsumable){

					PackName="freevid";

				}






				foreach (IPurchaseReceipt productReceipt in result) {

					//test
					Debug.Log("productID : "+productReceipt.productID);
					Debug.Log("purchaseDate : "+productReceipt.purchaseDate);
					Debug.Log("transactionID :  "+productReceipt.transactionID);

					GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
					if (null != google) {
						Debug.Log(google.purchaseState);
						Debug.Log(google.purchaseToken);
					}

					AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
					if (null != apple) {


						//test
						Debug.Log("originalTransactionIdentifier"+apple.originalTransactionIdentifier);
						Debug.Log("cancellationDate"+apple.cancellationDate);
						Debug.Log("quantity"+apple.quantity);
						Debug.Log("subscriptionExpirationDate"+apple.transactionID);

						Debug.Log("subscriptionExpirationDate"+apple.subscriptionExpirationDate);

						expDate=apple.subscriptionExpirationDate.ToString();
					}
				}
			} catch (IAPSecurityException) {
				Debug.Log("Invalid receipt, not unlocking content");
				return PurchaseProcessingResult.Complete;
			}
		}
		#endif

		//#if !UNITY_EDITOR





		if (canupload) {

			StartCoroutine (Paytoserver (payload, expDate, PackName));
		}

		//#endif

		// You should unlock the content here.

		// Indicate we have handled this purchase, we will not be informed of it again.x
		return PurchaseProcessingResult.Complete;

		//			// A consumable product has been purchased by this user.
		//			if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
		//			{
		//				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
		//				//ScoreManager.score += 100;
		//			}
		//
		//			// Or ... a non-consumable product has been purchased by this user.
		//			else if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
		//			{
		//				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));}// Or ... a subscription product has been purchased by this user.
		//			else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
		//			{
		//				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));}// Or ... an unknown product has been purchased by this user. Fill in additional products here.
		//			else 
		//			{
		//				Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));}// Return a flag indicating wither this product has completely been received, or if the application needs to be reminded of this purchase at next app launch. Is useful when saving purchased products to the cloud, and when that save is delayed.
		//			return PurchaseProcessingResult.Complete;
	}



	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing this reason with the user.
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",product.definition.storeSpecificId, failureReason));}




	class rec{

		public	string Store;
		public	string TransactionID;
		public	string 	Payload;


	}


	IEnumerator	Paytoserver(string pay,string expriydate,string date){
		savetoparents ();
		canupload = false;

		string url = "http://webgl.esy.es/newlinking/rec.php";



		WWWForm form = new WWWForm ();
		form.AddField ("receipt", pay);
		form.AddField ("expdate", expriydate);
		form.AddField ("pack", PackName);

		form.AddField ("cname", child.text);

		form.AddField ("pname", pname.text);
		form.AddField ("add", add.text);


form.AddField ("AccID", PlayerPrefs.GetInt ("AccID"));

	form.AddField ("AccID", "");
		form.AddField ("dob", month.text+"/"+day.text+"/"+year.text);
		form.AddField ("email", email.text);
		form.AddField ("scode", schooolscode);
		form.AddField ("platform", Platforms);

		form.AddField ("packdeatils", packdeatils);
		//form.AddField(
		//Debug.Log (url);
		www = new WWW (url,form);

		yield return www;


		Debug.Log (www.text);

		PlayerPrefs.SetString ("reg", "yes");
		PlayerPrefs.SetInt ("AccID", int.Parse(www.text));
	}






	public void BuyIapProduct(){
//		Debug.Log( child.text);
//
//		Debug.Log( pname.text);
//		Debug.Log( add.text);
//		//Debug.Log( dob.text);
//		Debug.Log(email.text);
		foreach (Text t in txtstobecheck) {

			if (t.text == "") {

				return;
			}
		}





		if (Application.platform == RuntimePlatform.Android)
			Platforms = "Android";
				else if(Application.platform == RuntimePlatform.IPhonePlayer)
			Platforms = "IPhone";
		
		else if(Application.platform==RuntimePlatform.OSXEditor)
			Platforms = "OSXEditor";
		


		switch (setpackname) {


		case "pack12":
			BuySubscription ();
			break;
		case "pack24":
			BuySubscription24 ();

			break;
		case "pack36":
			
			BuySubscription36 ();

			break;

		case "pack_free":

			BuyNonConsumable ();
			break;




		}



	}
	public void setFreecode(){
		setpackname="pack_free";

	}

	public void SetScodevalue(){

		schooolscode=scode.text;

	}
	void savetoparents(){


		PlayerPrefs.SetString (_day, day.text);
		PlayerPrefs.SetString (_month, month.text);
		PlayerPrefs.SetString (_year, year.text);
	

		PlayerPrefs.SetString (_cname, child.text);

		PlayerPrefs.SetString (_pname, pname.text);
		PlayerPrefs.SetString(_add, add.text);
		PlayerPrefs.SetString (_emailaddress, email.text);
}
}



