#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("w4esFYjU4GT1TOu0Q3iCh5626T6jfZ60CNqt7TdsIeqSf3BOM4yIpt1eUF9v3V5VXd1eXl/6M+XZVvVBiJSe4OTR0mlmr3ajrKGEaNOTPOTWKCc+A9RHWslXsTmSlP/hCSmtbW/dXn1vUllWddkX2ahSXl5eWl9cngeAz909t/Fz7f1apO+Msbn2FL8BSRWN12SFjwM0bTOl8dj6mehd59UJR6xo+HfIDOVKaNGnYco6ijM27j5a3cAPInekICn9Q12PVSZ8kWEM1GzT8lBsER4U7FaZlStOnSFKRWSLhb8L2/8JOrRbn2rKPaUAINMVsm+M3XHArxAK9qkdanSo679NFkRq1rQ10xhiQrheDGovBwn1MhBkEs2PjQH5af+Yol1cXl9e");
        private static int[] order = new int[] { 8,5,5,13,4,8,10,8,10,11,13,12,12,13,14 };
        private static int key = 95;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
