#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("L6A3bpWUmgiRK7uMtO5PppdB+Iyqi5yZz56QiZDb6ur2/7rT9Pm0qyRu6QF0SP6VUePVrkI4pGPiZfFSMkbkuK9Qv09DlUzxTji+uYttOzaFHxkfgQOn3a1oMwHaFLZOKwqIQlOD6G/HlE/lxQFov5kgzxXXx5drutnbqhibuKqXnJOwHNIcbZebm5v9FZIuum1RNra69eospZuqFi3ZVTE56wjdyc9bNbXbKWJheepXfDnW4KoYm+yqlJyZz4eVm5tlnp6ZmJvf5IXW8coM2xNe7viRihnbHakQGxibmpyTsBzSHG35/p+bqhtoqrCcWvmp7W2gnbbMcUCVu5RAIOmD1S++eHFLLepFld97vVBr9+J3fS+NjRGDE0Rj0fZvnTG4qphygqRiypNJhQtBhN3KcZ93xOMet3GsOM3Wz3YtgScJ2L6IsF2VhyzXBsT5UtEajZ6ciZjPyauJqoucmc+ekImQ2+rqlQenabHTsoBSZFQvI5RDxIZMUaf0/rr59fT+8+7z9fTpuvX8uu/p/6e8/boQqfBtlxhVRHE5tWPJ8MH+trr5/+ju8/zz+fvu/7rq9fbz+eOXnJOwHNIcbZebm5+fmpkYm5uaxp+amRiblZqqGJuQmBibm5p+CzOT7vL16PPu46uMqo6cmc+emYmX2+rq9v+6yPX17rrZ26qEjZeqrKquqLWqG1mckrGcm5+fnZiYqhssgBspK6rCdsCeqBbyKRWHRP/pZf3E/yacmc+HlJ6Mno6xSvPdDuyTZG7xF7r1/Lru8v+67vL/9Lr76ur28/n77vP88/n77v+6+OO6+/Tjuur76O7+r7mP0Y/DhykObWwGBFXKIFvCyhXpG/pcgcGTtQgoYt7SavqiBI9vqazAqvirkaqTnJnPnpyJmM/Jq4nI//bz+/T5/7r19Lru8vPpuvn/6LTaPG3d1+WSxKqFnJnPh7megqqM6Pv57vP5/7rp7vvu//f/9O7ptKqsA9a34i13FgFGae0BaOxI7arVW5KxnJufn52Ym4yE8u7u6umgtbXt00LsBamO/zvtDlO3mJmbmps5GJsajrFK890O7JNkbvEXtNo8bd3X5br79P66+f/o7vP88/n77vP19Lrq+Pb/uunu+/T+++j+uu7/6PfpuvuMqo6cmc+emYmX2+rq9v+6yPX17ur2/7rZ/+ju8/zz+fvu8/X0utvvqhieIaoYmTk6mZibmJibmKqXnJOdduejGRHJukmiXislANWQ8WWxZpLEqhibi5yZz4e6nhibkqoYm56q8/zz+fvu8/X0utvv7vL16PPu46tDrOVbHc9DPQMjqNhhQk/rBOQ7yOXbMgJjS1D8Br7xi0o5IX6BsFmF9v+60/T5tKu8qr6cmc+ekYmH2+qcqpWcmc+HiZubZZ6fqpmbm2Wqh+O6++np7/f/6br7+fn/6u779Pn/vKq+nJnPnpGJh9vq6vb/utn/6O4PBOCWPt0RwU6MralRXpXXVI7zS+3ttPvq6vb/tPn197X76ur2//n7r6irrqqprMCNl6mvqqiqo6irrqrDPZ+T5o3azIuE7kktEbmh3TlP9bAc0hxtl5ubn5+aqvirkaqTnJnPyjAQT0B+ZkqTna0q7++7");
        private static int[] order = new int[] { 23,9,50,54,49,19,13,21,24,28,25,34,57,53,16,15,59,50,22,43,56,44,47,59,43,53,37,37,30,41,39,56,55,43,38,56,46,48,56,48,55,45,42,57,49,59,51,57,56,58,50,54,59,55,55,57,57,58,59,59,60 };
        private static int key = 154;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
