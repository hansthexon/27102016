﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PolitePanelScript : MonoBehaviour {

	string PlanetScore="PlanetScore";

	public Text ScoreText;
	//public GameObject letmeshit;
	public GameObject mainScreen;
	public bool replay;
	string vivdeoname;
	public GameObject ReplayPanel;
	public GameObject PopupsPanel;
	public GameObject PolitePanel;
	public GameObject PoliteSky;
	public GameObject ScorePanel;
	public GameObject[] PanelstBeClosed;

	public GameObject videoPrefab;

		GameObject videoManager;
	 MediaPlayerCtrl player;
	DataService ds;
	Code codeobject;

	public GameObject[] PoliteButtons;






	public GameObject StarBar;
	public GameObject PlanetBar;
	// Use this for initialization
	void Start () {

		replay = true;
		ds = new DataService ("arapp.sqlite");

		foreach (GameObject g in PoliteButtons) {


			fetchPolCodeFromDb (g.name);
			if (codeobject.PolitePlanet == "Y") {


				g.transform.GetChild (0);

			}
				}
	}


	void fetchPolCodeFromDb(string filename){

		codeobject=ds.getVideo(filename);


	}


	public void OpenPoliteVideo(string filename){
		var code=ds.getVideo(filename);
		vivdeoname=code.SelfVideoFile;

	

			filename	=	vivdeoname ;


             player.Load ("file://" + Application.persistentDataPath + "/" + vivdeoname + ".mp4");
//player.OnEnd += SetScore;
			player.OnReady += PlayVideo;
            player.OnEnd += showreplyscreen;

		
	}



	void PlayVideo(){

		foreach (GameObject g  in PanelstBeClosed) {

			g.SetActive (false);
		}
		player.Play ();

	}




	void 	showPopscreen(){


		PopupsPanel.SetActive (true);


	}


	void showreplyscreen(){
		player.UnLoad ();
		player.OnEnd -= showreplyscreen;

	ReplayPanel.SetActive (true);
		//StartCoroutine (disabelle ());



		//Destroy (player);
	//	PoliteSky.SetActive(true);
	//	ReplayPanel.SetActive (true);


	}

	public void Replay(){

		player.Load ("file://" + Application.persistentDataPath + "/" + vivdeoname + ".mp4");
	
		player.OnReady += PlayVideo;


		player.OnEnd += showreplyscreen;


	}

	public void Backreplay(){
		
	//	PoliteSky.SetActive (true);

	//	ReplayPanel.SetActive (false);
		PopupsPanel.SetActive (true);

	}

	public void BackPopUp(){




	}

	public void ScorePanelBack(){

		Debug.Log ("Tester");
		mainScreen.SetActive (true);

//
//		PolitePanel.SetActive (false);
//		PolitePlanet.SetActive (false);
		ScorePanel.SetActive (false);
      ReplayPanel.SetActive (false);


		StarBar.SetActive (true);
		PlanetBar.SetActive (true);

		Destroy (player);
		Destroy (videoManager);


	}

	public void OpenScorePanel(){
		ReplayPanel.SetActive (false);
		PopupsPanel.SetActive (false);
		ScorePanel.SetActive (true);


		if (codeobject.PoliteFirst == "Y") {

			ScoreText.text ="+" +codeobject.SelfFirstScore.ToString();
			codeobject.PoliteFirst = "N";
			ds.UpdateFirst (codeobject);
			PlayerPrefs.SetInt (PlanetScore,PlayerPrefs.GetInt (PlanetScore)+codeobject.SelfFirstScore);

		} else {
			ScoreText.text = "+" +codeobject.SelfRescanScore.ToString();

			PlayerPrefs.SetInt (PlanetScore,PlayerPrefs.GetInt (PlanetScore)+codeobject.SelfRescanScore);

		}

	}

	public void ClosePopup(){
		PoliteSky.SetActive (true);
		ReplayPanel.SetActive (false);

		PopupsPanel.SetActive (false);

		Destroy (player);
		Destroy (videoManager);


	}

	public void ClosePolitePanel(){

		PoliteSky.SetActive (false);

		mainScreen.SetActive (true);
		StarBar.SetActive (true);
		PlanetBar.SetActive (true);

	}

	public void OpenPolitePanel(){

		PolitePanel.SetActive (true);


		foreach (GameObject g in PoliteButtons) {


			fetchPolCodeFromDb (g.name);

			if (codeobject.Purchased == "Y") {
				if (codeobject.PolitePlanet == "Y") {


					g.transform.GetChild (0).gameObject.SetActive (true);

				}

			}
		}
		videoManager =	Instantiate (videoPrefab) as GameObject;
		player = videoManager.GetComponent<MediaPlayerCtrl> ();
	}



	IEnumerator disabelle(){
		
		ReplayPanel.SetActive (false);
		yield	return new WaitForSeconds (.2f);

		ReplayPanel.SetActive (false);


	}

}
