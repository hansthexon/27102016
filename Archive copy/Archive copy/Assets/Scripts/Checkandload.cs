﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using LitJson;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using System;

public class Checkandload : MonoBehaviour {

	public GameObject loadbutton;
	CodeTest[] vr;
	List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
	float  perdownload;
	public 	Image progressbar;
	List<string> tobedownloaded;
	bool wasthereadownlaod=false;
	bool canpass=false;
//	public GameObject nextlevel;
	int i=0;
	public Text updatetext;
	string savepath;
	AsyncOperation asc;
	IEnumerable<Code> purchased;

	Code[] cr;
	// Use this for initialization
	DataService ds;
	void Start () {


		progressbar = progressbar.GetComponent<Image> ();
		tobedownloaded = new List<string> ();
	updatetext=	updatetext.GetComponent<Text> ();


		ds = new DataService ("arapp.sqlite");
		purchased = ds.GetallCodes ();



		//		foreach(var v in purchased){
		//
		//			Debug.Log (v.CodeARimage);
		//
		//		}
		//Debug.Log (purchased.ToString ());
		StartCoroutine(Download ());

		//SceneManager.LoadScene (1);
	}

	// Update is called once per frame
	void Update () {
		
		if (canpass) {
			if (tobedownloaded.Count > 0) {
				wasthereadownlaod = true;

				StartCoroutine(dowloadVideo(tobedownloaded.First ()));
					canpass=false;
				//start a couroutine for download 
				//stop the execution of update thread

					}else{

						nextscene();
						canpass=false;
					}


		}

	}




	IEnumerator Download(){
		
		if (!PlayerPrefs.HasKey ("AccID")) {
			nextscene ();

			
		}else{
		WWWForm form = new WWWForm ();
			form.AddField ("AccID","1234");
		//form.AddField ("AccID", PlayerPrefs.GetInt ("AccID"));
	


			WWW	www = new WWW ("http://webgl.esy.es/newlinking/index.php", form);

		while (!www.isDone) {



			yield return null;


		}
		vr = JsonMapper.ToObject<CodeTest[]> (www.text);


		//Updatemytest (	vr[0].CodeARimage+"  "+purchased.ElementAt(1).CodeARimage);


		//JsonReader json = new JsonReader (www.text);
		//	var test=	json
		///Json.Deserialize<CodeTest> (www.text);

		foreach (CodeTest v in vr) {
			//	Updatemytest ("in first loop");

			foreach (Code c in purchased) {



				//Updatemytest ("in second loop"+i);
				if (v.CodeARimage == c.CodeARimage) {
					Updatemytest ("match");


					if (c.Purchased != "Y") {


						//	Debug.Log (c.VideoFile);

						//Updatemytest ("dowload "+c.VideoFile);

						tobedownloaded.Add (c.VideoFile);
						if (c.PolitePlanet == "Y") {

							tobedownloaded.Add (c.SelfVideoFile);

						}
						//StartCoroutine (dowloadVideo (c.VideoFile));





					}

				}


				if (v.CodeARimage == c.CodeARimage) {
					
				

					if (c.Version < Int32.Parse (v.Version)) {

						Debug.Log ("Version" + c.VideoFile);

						//Updatemytest ("dowload "+c.VideoFile);
						list.Add (new KeyValuePair<string,int> (c.CodeARimage, Int32.Parse (v.Version)));
						tobedownloaded.Add (c.VideoFile);
						if (c.PolitePlanet == "Y") {
							list.Add (new KeyValuePair<string,int> (c.CodeARimage, Int32.Parse (v.Version)));
							tobedownloaded.Add (c.SelfVideoFile);

						}
						//StartCoroutine (dowloadVideo (c.VideoFile));





					}

				}
			}

		}




	

		perdownload = 1 / (float)tobedownloaded.Count;

		Debug.Log (perdownload.ToString ());
		canpass = true;

	}
	}


	IEnumerator dowloadVideo(string filename){

		//Updatemytest ("downloading file "+filename);

		Debug.Log (filename);
		var www = new WWW("http://webgl.esy.es/details/"+filename+".mp4");



		while (!www.isDone) {



			yield return null;



		}
		Updatemytest ("savepath "+savepath);

		File.WriteAllBytes (Application.persistentDataPath + "/" + filename + ".mp4", www.bytes);



		Updatemytest ("updating Db");

		UpdateDb (filename);

	}
	void 	UpdateDb(string filename){

		var codevar=ds.getVideo (filename);


		codevar.Purchased="Y";
	//	codevar.Version=
		foreach (var v in list) {

			if (v.Key == codevar.CodeARimage) {

				codevar.Version = v.Value;
			//	codevar.FirstScore=


				foreach(var k in vr){
					
					if (codevar.CodeARimage == k.CodeARimage) {
						codevar.RescanScore = Int32.Parse(k.RescanScore);
						codevar.SelfRescanScore = Int32.Parse(k.SelfRescanScore);
						codevar.Content = k.Content;
						codevar.Country = k.Country;
						codevar.ForMonth = k.ForMonth;
						codevar.Type = k.Type;
						codevar.Type2 = k.Type2;


					}


				}

			

			}


		}
			
		#if !UNITY_EDITOR
		ds.UpdateFirst (codevar);
		#endif



	
	
//		nextlevel.SetActive (true);
	
		tobedownloaded.Remove (filename);
		//SceneManager.LoadScene (1);
		IncreaseBar();
		canpass = true;
	}

	void Updatemytest(string text){
		updatetext.text=	updatetext.text+text;
	}

	public void nextscene(){

		if (wasthereadownlaod) {

			SceneManager.LoadScene (1);
		} else {
			
//			if (Application.platform==RuntimePlatform.IPhonePlayer) {
//				loadbutton.SetActive (true);
//
//			} else {

			StartCoroutine (LoadLevelWithBar (1));
		}
		}




	public void loadlevelwith(){


		SceneManager.LoadScene (1);
	}

	//what is the size on the list 

	void IncreaseBar(){

	// get numer 	tobedownloaded.Count

		progressbar.fillAmount += (float)perdownload;


	}


	public	IEnumerator LoadLevelWithBar (int level)
	{


		asc = SceneManager.LoadSceneAsync (1);
	

		while (!asc.isDone)
		{


			progressbar.fillAmount = asc.progress;
			yield return null;
		}
	}
}
