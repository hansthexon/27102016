﻿using UnityEngine;
using System.Collections;
using SQLite4Unity3d;
public class RoomObject {

	[PrimaryKey,AutoIncrement,Unique]
	public string ObjId { get; set; }
	public string ObjPNG { get; set; } 
	public string Room { get; set; } 
	public string ReachScore { get; set; } 
	public string Earned { get; set; } 
	public string PlayerInRoomPosXY { get; set; } 


}
