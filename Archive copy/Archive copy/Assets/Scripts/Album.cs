﻿using SQLite4Unity3d;



public class Album {

	[PrimaryKey, AutoIncrement]
	public int AlbumId { get; set; }
	public string Title { get; set; }
	public string ArtistId { get; set; }


//	public override string ToString ()
//	{
//		//return string.Format ("[Person: AlbumId={0}, Title={1},  ArtistId={2}]", AlbumId, Title, ArtistId);
//
//
//}
}