﻿using UnityEngine;
using System.Collections;

public class Game_anim : MonoBehaviour
{
    public GameObject gameButton;

    Animator gameAnim;
    // Use this for initialization
    void Start()
    {
        gameAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void gameAnimate()
    {
        gameAnim.SetTrigger("GameClick_Trigger");
    }
}
