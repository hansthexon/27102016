﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackScript : MonoBehaviour {


	public	bool canopenparent;
	public	Text day;

	public	Text month;
	public	Text year;
	public	Text child;
	public	Text pname;
	public	Text  addr;
	public Text emailaddress;

	public GameObject ParentCirner;
	public GameObject Slidec;
	public GameObject SldieE;
	public GameObject threemenu;

	public GameObject membershipdetails;
		public GameObject purch;

	public GameObject togg;
	// Use this for initialization


	public	Purchaser pur;

	public void ParentBack(){
		
ParentCirner.SetActive (false);

	}
	public void SlideCBack(){

		Slidec.SetActive (false);
		ParentCirner.SetActive (true);
	}

	public void SlideEBack(){

		SldieE.SetActive (false);
		Slidec.SetActive (true);
	}
	public void threemenuback(){

		if (PlayerPrefs.GetString ("reg", "no") == "yes") {
			threemenu.SetActive (false);
			ParentCirner.SetActive (true);
		} else {

			threemenu.SetActive (false);
			Slidec.SetActive (true);
		}


	}



	public void nextpage(){

		membershipdetails.SetActive (true);
		threemenu.SetActive (false);
	}

	public void backReg(){

		if (canopenparent) {


			PlayerPrefs.SetString ("day", day.text);
			PlayerPrefs.SetString ("month", month.text);
			PlayerPrefs.SetString ("year", year.text);


			PlayerPrefs.SetString ("cname", child.text);

			PlayerPrefs.SetString ("pname", pname.text);
			PlayerPrefs.SetString ("add", addr.text);
			PlayerPrefs.SetString ("emailaddress", emailaddress.text);

			canopenparent = false;
			membershipdetails.SetActive (false);
			togg.SetActive (true);
			return;

		}


		if (pur.setpackname == "pack_free") {
			membershipdetails.SetActive (false);
			SldieE.SetActive (true);
		} else {
			membershipdetails.SetActive (false);
			threemenu.SetActive (true);

		}
	}

	public void Membership(){
		SldieE.SetActive (false);
		membershipdetails.SetActive (true);
	//	threemenu.SetActive (false);
	}
	public void enablepurchase(){

		purch.GetComponent<Purchaser> ().enabled=true;

	}

	public void enableparentcorner(){
		
		membershipdetails.SetActive (true);

		canopenparent = true;
		togg.SetActive (false);
	
	}

}
