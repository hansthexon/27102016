using UnityEngine;
using System.Collections;

public class Robot_anim : MonoBehaviour
{
    public GameObject robotButton;

    Animator robotAnim;
    // Use this for initialization
    void Start()
    {
        robotAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void robotAnimate()
    {
        robotAnim.SetTrigger("RobotClick_Trigger");
    }
}
