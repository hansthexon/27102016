using UnityEngine;
using System.Collections;

public class Ball_anim : MonoBehaviour
{
    public GameObject ballButton;

    Animator ballAnim;
    // Use this for initialization
    void Start()
    {
        ballAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ballAnimate()
    {
        ballAnim.SetTrigger("BallClick_Trigger");
    }
}
